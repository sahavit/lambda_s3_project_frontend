import Vue from  'vue';
import Vuex from 'vuex';

import images from './modules/images';

//load vuex
Vue.use(Vuex);

//create store

export default new Vuex.Store({
    modules:{
        images
    }
})