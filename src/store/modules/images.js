import axios from "axios"

const state={
     images : []

}

const getters={
    allImages: (state)=> state.images

}

const actions={
    async fetchImages({commit}){
        const res = await axios.get('/all');
        console.log(res.data)
        commit('setImages', res.data);

    }
}

const mutations={
    setImages: (state, images)=>(state.images = images)
  
}

export default{
    state,
    getters,
    actions,
    mutations
}